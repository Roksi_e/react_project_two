import dashboard from "../icon/dashboard.svg";
import revenue from "../icon/revenue.svg";
import notification from "../icon/notification.svg";
import analytics from "../icon/analytics.svg";
import inventory from "../icon/inventory.svg";
import logout from "../icon/logout.svg";
import lightmode from "../icon/lightmode.svg";

const data = [
  {
    image: dashboard,
    name: "Dashboard",
  },
  {
    image: revenue,
    name: "Revenue",
  },
  {
    image: notification,
    name: "Notifications",
  },
  {
    image: analytics,
    name: "Analytics",
  },
  {
    image: inventory,
    name: "Inventory",
  },
];

const dataFooter = [
  {
    image: logout,
    name: "Logout",
    element: "",
  },
  {
    image: lightmode,
    name: "Light mode",
    element: "",
  },
];

export { data, dataFooter };
