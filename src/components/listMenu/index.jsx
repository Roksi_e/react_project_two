import React from "react";
import ContentMenu from "../contentMenu";

import { data } from "../../data/data";

import "./listMenu.css";

function ListMenu({ className }) {
  return (
    <div className={className}>
      {Array.isArray(data)
        ? data.map((e, i) => {
            return (
              <ContentMenu
                className={className + "_content-menu"}
                data={e}
                key={i + "data"}
              />
            );
          })
        : console.error("Error")}
    </div>
  );
}

export default ListMenu;
