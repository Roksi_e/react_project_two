import React from "react";
import Chevron from "../chevron";
import Checkbox from "../checkbox";

function ContentMenu({ data, className }) {
  const { image, name, element } = data;
  if (
    className === "list-menu_content-menu" ||
    className === "footer_content-menu"
  ) {
    return (
      <div className={className}>
        <Chevron className="list-menu_icon" icon={image} />
        <div className="list-menu_name">
          <a href="#">{name}</a>
        </div>
        {element === "" || element === undefined || element === null ? null : (
          <div className="footer_icon">
            <Checkbox className="checkbox" icon={element} />
          </div>
        )}
      </div>
    );
  } else {
    return (
      <div className={className}>
        <Chevron className="list-menu_icon" icon={image} />
        {element === "" || element === undefined || element === null ? null : (
          <div className="footer_icon">
            <Checkbox className="checkbox" icon={element} />
          </div>
        )}
      </div>
    );
  }
}

export default ContentMenu;
