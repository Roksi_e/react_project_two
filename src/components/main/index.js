import React from "react";
import Header from "../header";
import Search from "../search";
import ListMenu from "../listMenu";
import Footer from "../footer";

function Main() {
  return (
    <div className="main_box">
      <div className="main">
        <Header className="header" />
        <Search className="menu_search" />
        <ListMenu className="list-menu" />
        <Footer className="footer" />
      </div>
      <div className="white-box"></div>
    </div>
  );
}

export default Main;
