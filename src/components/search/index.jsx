import React from "react";
import Chevron from "../chevron";

import lupa from "../../icon/lupa.svg";

import "./search.css";

function Search({ className }) {
  if (className === "menu_search") {
    return (
      <div className={className}>
        <Chevron className="menu-search_lupa" icon={lupa} />

        <input type="search" id="menu_input" placeholder="Search..." />
      </div>
    );
  } else {
    return (
      <div className={className}>
        <Chevron className="menu-search_lupa" icon={lupa} />

        <input type="search" id="menu_input_small" />
      </div>
    );
  }
}

export default Search;
