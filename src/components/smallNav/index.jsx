import React from "react";
import Header from "../header";
import Search from "../search";
import ListMenu from "../listMenu";
import Footer from "../footer";

function SmallNav() {
  return (
    <div className="main_small">
      <Header className="header_small" />
      <Search className="menu_search_small" />
      <ListMenu className="list-menu_small" />
      <Footer className="footer_small" />
    </div>
  );
}

export default SmallNav;
