import React from "react";
import ContentMenu from "../contentMenu";
import Chevron from "../chevron";
import Checkbox from "../checkbox";

import { dataFooter } from "../../data/data";
import moon from "../../icon/moon.svg";

import "./footer.css";

dataFooter[1].element = moon;

function Footer({ className }) {
  if (className === "footer") {
    return (
      <div className={className}>
        {Array.isArray(dataFooter)
          ? dataFooter.map((e, i) => {
              return (
                <ContentMenu
                  className={className + "_content-menu"}
                  data={e}
                  key={i + "data"}
                />
              );
            })
          : console.error("Error")}
      </div>
    );
  } else {
    return (
      <div className={className}>
        <Chevron className="list-menu_icon" icon={dataFooter[0].image} />
        <div className="footer_icon">
          <Checkbox className="checkbox" icon={dataFooter[1].element} />
        </div>
      </div>
    );
  }
}

export default Footer;
// {<ContentMenu
//   className={className + "_content-menu"}
//   data={dataFooter[0]}
// />}
// {      <div className={className}>
//         {Array.isArray(dataFooter)
//           ? dataFooter.map((e, i) => {
//               return (
//                 <ContentMenu
//                   className={className + "_content-menu"}
//                   data={e}
//                   key={i + "data"}
//                 />
//               );
//             })
//           : console.error("Error")}
//       </div>}
