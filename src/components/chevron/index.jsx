import React from "react";

function Chevron({ icon, className }) {
  return (
    <div className={className}>
      <img src={icon} alt="vector" />
    </div>
  );
}

export default Chevron;
