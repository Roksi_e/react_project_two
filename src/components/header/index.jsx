import React from "react";
import Chevron from "../chevron";

import vector from "../../icon/vector.svg";
import vector2 from "../../icon/vector2.svg";

import "./header.css";

function Header({ className }) {
  if (className === "header") {
    return (
      <div className={className}>
        <div className="header_avatar">AF</div>
        <div className="header_block">
          <div className="header_user-name">AnimatedFred</div>
          <div className="header_user-mail">animated@demo.com</div>
        </div>
        <Chevron className="header_icon" icon={vector} />
      </div>
    );
  } else {
    return (
      <div className={className}>
        <div className="header_avatar">AF</div>
        <Chevron className="header_icon_small" icon={vector2} />
      </div>
    );
  }
}

export default Header;
