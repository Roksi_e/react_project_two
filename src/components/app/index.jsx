import React from "react";
import Main from "../main";
import SmallNav from "../smallNav";

import "./app.css";

function App() {
  return (
    <>
      <Main />
      <SmallNav />
    </>
  );
}

export default App;
