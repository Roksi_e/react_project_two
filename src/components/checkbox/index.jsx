import React from "react";

const Checkbox = ({ icon, className }) => {
  return (
    <div className={className}>
      <input type="checkbox" id="checkbox_moon" />
      <img src={icon} alt="" />
    </div>
  );
};

export default Checkbox;
